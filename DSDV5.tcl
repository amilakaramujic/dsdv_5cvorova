# This script is created by NSG2 beta1
# <http://wushoupong.googlepages.com/nsg>

#===================================
#     Simulation parameters setup
#===================================
#Antenna/OmniAntenna set Gt_ 1              ;#Transmit antenna gain
#Antenna/OmniAntenna set Gr_ 1              ;#Receive antenna gain
#Phy/WirelessPhy set L_ 1.0                 ;#System Loss Factor
#Phy/WirelessPhy set freq_ 2.472e9          ;#channel
#Phy/WirelessPhy set bandwidth_ 11Mb        ;#Data Rate
#Phy/WirelessPhy set Pt_ 0.031622777        ;#Transmit Power
#Phy/WirelessPhy set CPThresh_ 10.0         ;#Collision Threshold
#Phy/WirelessPhy set CSThresh_ 5.011872e-12 ;#Carrier Sense Power
#Phy/WirelessPhy set RXThresh_ 5.82587e-09  ;#Receive Power Threshold
#Mac/802_11 set dataRate_ 11Mb              ;#Rate for Data Frames
#Mac/802_11 set basicRate_ 1Mb              ;#Rate for Control Frames

set val(chan)   Channel/WirelessChannel    ;# channel type
set val(prop)   Propagation/TwoRayGround   ;# radio-propagation model
set val(netif)  Phy/WirelessPhy            ;# network interface type
set val(mac)    Mac/802_11                 ;# MAC type
set val(ifq)    Queue/DropTail/PriQueue    ;# interface queue type
set val(ll)     LL                         ;# link layer type
set val(ant)    Antenna/OmniAntenna        ;# antenna model
set val(ifqlen) 200                        ;# max packet in ifq
set val(nn)     5                          ;# number of mobilenodes
set val(rp)     DSDV                      ;# routing protocol
set val(x)      500                      ;# X dimension of topography
set val(y)      500                      ;# Y dimension of topography
set val(stop)   60.0                         ;# time of simulation end

#===================================
#        Initialization        
#===================================
#Create a ns simulator
set ns [new Simulator]

#Setup topography object
set topo       [new Topography]
$topo load_flatgrid $val(x) $val(y)
create-god $val(nn)

#Open the NS trace file
set tracefd [open dsdv_out5.tr w]
$ns trace-all $tracefd
$ns use-newtrace
#Open the NAM trace file
set namtrace [open dsdv_out5.nam w]
$ns namtrace-all-wireless $namtrace $val(x) $val(y)
set chan [new $val(chan)];#Create wireless channel

set topo [new Topography]
$topo load_flatgrid $val(x) $val(y)

create-god $val(nn)
set chan_1 [new $val(chan)]
#===================================
#     Mobile node parameter setup
#===================================
$ns node-config -adhocRouting  $val(rp) \
                -llType        $val(ll) \
                -macType       $val(mac) \
                -ifqType       $val(ifq) \
                -ifqLen        $val(ifqlen) \
                -antType       $val(ant) \
                -propType      $val(prop) \
                -phyType       $val(netif) \
                -channel       $chan \
                -topoInstance  $topo \
             

#===================================
#        Nodes Definition        
#===================================
#Create 5 nodes
for {set i 0} {$i < 5} { incr i } {
            set n($i) [$ns node]
            $n($i) random-motion 1 
            $n($i) color red
            $ns at 0.0 "$n($i) color red"
            $ns initial_node_pos $n($i) 20
    }



#===================================
#        Generate movement          
#===================================


$ns at 0.0 "$n(0) setdest 91.7 68.0 10000.0"
$ns at 0.5 "$n(1) setdest 28.4 168.3 10000.0"
$ns at 0.7 "$n(2) setdest 27.3 227.4 10000.0"
$ns at 5.0 "$n(3) setdest 20.05 3.98 10000.0"
$ns at 0.0 "$n(4) setdest 30.8 435.3 10000.0"


$ns at 10.3 "$n(2) setdest 28.4 168.3 50.0"
$ns at 10.3 "$n(0) setdest 166.3 64.0 50.0"
$ns at 12.3 "$n(1) setdest 26.5 113.7 50.0"
$ns at 16.3 "$n(3) setdest 101.2 4.0 50.0"
$ns at 15.3 "$n(4) setdest 29.9 372.5 50.0"

$ns at 24.8 "$n(0) setdest 135.9 105.3 50.0"
$ns at 29.8 "$n(2) setdest 26.5 113.7 50.0"
$ns at 25.8 "$n(3) setdest 101.2 4.0 50.0"
$ns at 25.8 "$n(1) setdest 23.6 63.1 50.0"
$ns at 23.8 "$n(4) setdest 30.3 293.5 50.0"

$ns at 40.0 "$n(4) setdest 104.5 226.2 500.0"
$ns at 40.0 "$n(0) setdest 135.9 105.3 500.0"
$ns at 40.0 "$n(2) setdest 242.8 437.0 1000.0"
$ns at 34.0 "$n(1) setdest 418.9 215.4 100.0"

$ns at 50.0 "$n(0) setdest 91.7 68.0 10000.0"
$ns at 50.5 "$n(1) setdest 28.4 168.3 10000.0"
$ns at 50.7 "$n(2) setdest 27.3 227.4 10000.0"
$ns at 57.0 "$n(3) setdest 20.05 3.98 10000.0"
$ns at 50.0 "$n(4) setdest 30.8 435.3 10000.0"
#===================================
#        Agents Definition        
#===================================

##===================================
#        Agents Definition        
#===================================


set udp6 [new Agent/UDP]
$ns attach-agent $n(2) $udp6
set sink8 [new Agent/LossMonitor]
$ns attach-agent $n(1) $sink8
$ns connect $udp6 $sink8

#Setup a CBR Application over UDP connection
set cbr4 [new Application/Traffic/CBR]
$cbr4 attach-agent $udp6
$cbr4 set packetSize_ 512
$cbr4 set rate_ 0.1kbps
$cbr4 set random_ 1
$ns at 1.0 "$cbr4 start"
$ns at 60.0 "$cbr4 stop"


set udp9 [new Agent/UDP]
$ns attach-agent $n(3) $udp9
set sink10 [new Agent/LossMonitor]
$ns attach-agent $n(4) $sink10

$ns connect $udp9 $sink10

#Setup a CBR Application over UDP connection
set cbr1 [new Application/Traffic/CBR]
$cbr1 attach-agent $udp9
$cbr1 set packetSize_ 512
$cbr1 set rate_ 0.1kbps
$cbr1 set random_ 1
$ns at 1.0 "$cbr1 start"
$ns at 60.0 "$cbr1 stop"


set udp11 [new Agent/UDP]
$ns attach-agent $n(0) $udp11
set sink14 [new Agent/LossMonitor]
$ns attach-agent $n(1) $sink14

$ns connect $udp11 $sink14

#Setup a CBR Application over UDP connection
set cbr2 [new Application/Traffic/CBR]
$cbr2 attach-agent $udp11
$cbr2 set packetSize_ 512
$cbr2 set rate_ 0.1kbps
$cbr2 set random_ 1
$ns at 1.0 "$cbr2 start"
$ns at 60.0 "$cbr2 stop"

set udp12 [new Agent/UDP]
$ns attach-agent $n(4) $udp12
set sink13 [new Agent/LossMonitor]
$ns attach-agent $n(0) $sink13

$ns connect $udp12 $sink13


#Setup a CBR Application over UDP connection
set cbr3 [new Application/Traffic/CBR]
$cbr3 attach-agent $udp12
$cbr3 set packetSize_ 512
$cbr3 set rate_ 0.1kbps
$cbr3 set random_ 1
$ns at 1.0 "$cbr3 start"
$ns at 60.0 "$cbr3 stop"

#===================================
#        Termination        
#===================================
#Define a 'finish' procedure
proc finish {} {
    global ns tracefd namtrace
    $ns flush-trace
    close $tracefd
    close $namtrace
    exec nam dsdv_out5.nam &
    exit 0
}
for {set i 0} {$i < $val(nn) } { incr i } {
    $ns at $val(stop) "\$n($i) reset"
}
$ns at $val(stop) "$ns nam-end-wireless $val(stop)"
$ns at $val(stop) "finish"
$ns at $val(stop) "puts \"done\" ; $ns halt"
$ns run


